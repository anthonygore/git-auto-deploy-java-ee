package servletPackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author anthonygore
 */
@WebServlet(name = "Deploy", urlPatterns = {"/Deploy"})
public class Deploy extends HttpServlet {

    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:mysql://localhost/deploy";

    //  Database credentials
    private String USER;
    private String PASS;

    // Logger
    static final Logger LOGGER = Logger.getLogger(Deploy.class);

    // An array to store info to be logged to the database at the end.
    // Should be in (columnName, value) pairs with index (0, 1)
    private ArrayList<String[]> dbLog;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        LOGGER.info("Deploy app initialised.");

        // Create database credentials
        loadProperties();

        // Grab JSON
        String json = "[" + request.getParameter("payload") + "]";

        // Initialise dbLog
        this.dbLog = new ArrayList<String[]>();
        
        // This is an example payload from Bitbucket, can be used for debugging
        //String json = "[{\"repository\": {\"website\": \"\", \"fork\": false, \"name\": \"Aussie Family Survey\", \"scm\": \"git\", \"owner\": \"anthonygore\", \"absolute_url\": \"/anthonygore/aussie-family-survey/\", \"slug\": \"aussie-family-survey\", \"is_private\": true}, \"truncated\": false, \"commits\": [{\"node\": \"fbb4af161cd5\", \"files\": [{\"type\": \"modified\", \"file\": \"README.md\"}], \"raw_author\": \"Anthony Gore <anthony@alliedpublishers.com.au>\", \"utctimestamp\": \"2014-07-11 05:59:18+00:00\", \"author\": \"anthonygore\", \"timestamp\": \"2014-07-11 07:59:18\", \"raw_node\": \"fbb4af161cd558ad978d20077ea9b4e67de3ac99\", \"parents\": [\"a4b69d892376\"], \"branch\": \"master\", \"message\": \"Updated readme\\n\", \"revision\": null, \"size\": -1}], \"canon_url\": \"https://bitbucket.org\", \"user\": \"anthonygore\"}]";
        // Add the payload to the database log
        this.dbLog.add(new String[]{"payload", json});

        // Parse
        JSONParser parser = new JSONParser();

        // Variables
        String slug = "";
        String branch = "";
        String author = "";

        try {
            Object obj = parser.parse(json);
            JSONArray arr = (JSONArray) obj;
            JSONObject all = (JSONObject) arr.get(0);

            // Get the slug
            JSONObject repository = (JSONObject) all.get("repository");
            slug = (String) repository.get("slug");

            // Get the branch
            arr = (JSONArray) all.get("commits");
            JSONObject commits = (JSONObject) arr.get(0);
            branch = (String) commits.get("branch");

            // Get the author
            author = (String) commits.get("author");
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage());
        }

        // Add these variables to the logs
        this.dbLog.add(new String[]{"slug", slug});
        this.dbLog.add(new String[]{"branch", branch});
        this.dbLog.add(new String[]{"author", author});
        LOGGER.info("Variables extracted from payload. Slug: " + slug + ", branch: " + branch + ", author: " + author);

        // Check if this commit requires deployment
        String folder = this.getFolder(slug, branch, author);
        if (folder != null) {
            LOGGER.info("Folder found: " + folder);
            this.deploy(folder);
        } else {
            LOGGER.info("No folder found, no deployment taking place.");
            this.dbLog.add(new String[]{"isSuccess", "0"});
        }

        //Write the dbLog to the database
        logToDatabase(this.dbLog);

    }

    /**
     * Writes the dbLog array to the database
     *
     * @param colVal
     */
    private void logToDatabase(ArrayList<String[]> colVal) {
        // Make database connection
        Connection conn = null;
        Statement stmt = null;

        try {
            // Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // Open a connection
            conn = DriverManager.getConnection(DB_URL, this.USER, this.PASS);

            // Construct query
            String q1 = "";
            String q2 = "";
            for (String[] arr : colVal) {
                q1 += arr[0] + ", ";
                q2 += "'" + arr[1] + "', ";
            }

            // Remove the last comma
            q1 = q1.substring(0, q1.length() - 2);
            q2 = q2.substring(0, q2.length() - 2);
            String sql = "INSERT INTO log (" + q1 + ") VALUES(" + q2 + ");";

            //Execute query
            stmt = conn.createStatement();
            stmt.execute(sql);

            //Clean-up environment
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            LOGGER.error("SQL exception: " + se.getMessage());
        } catch (ClassNotFoundException e) {
            LOGGER.error("Loading MySQL driver failed: " + e.getMessage());
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
                LOGGER.error("Error closing statement: " + se2.getMessage());
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                LOGGER.error("Closing connection failed: " + se.getMessage());
            }
        }
    }

    /**
     *
     * @param slug
     * @param branch
     * @param author
     * @return
     */
    private String getFolder(String slug, String branch, String author) {
        // Make database connection
        Connection conn = null;
        Statement stmt = null;
        String folder = null;

        try {
            // Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //Execute query
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT folder FROM repositories WHERE slug='" + slug
                    + "' AND (branch='" + branch + "' OR branch='any') AND (author='"
                    + author + "' OR author='any')";
            ResultSet rs = stmt.executeQuery(sql);

            //Extract data from first result
            while (rs.next()) {
                folder = rs.getString("folder");
            }

            //Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            LOGGER.error("SQL exception: " + se.getMessage());
        } catch (ClassNotFoundException e) {
            //Handle errors for Class.forName
            LOGGER.error("Loading MySQL driver failed: " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
                LOGGER.error("Error closing statement: " + se2.getMessage());
            }// nothing we can do
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                LOGGER.error("Closing connection failed: " + se.getMessage());
            }
        }
        return folder;
    }

    /**
     * Deploys
     *
     * @param folder
     */
    private void deploy(String folder) {

        //Build commands 
        List<String> commands = new ArrayList<String>();
        commands.add("git --git-dir=" + folder + "/.git --work-tree=" + folder + " reset --hard HEAD 2>&1");
        commands.add("git --git-dir=" + folder + "/.git --work-tree=" + folder + " pull");
        commands.add("chmod -R og-rx " + folder + "/.git");
        commands.add("find " + folder + " -type f -exec chmod 644 {} \\;");
        commands.add("find " + folder + " -type d -exec chmod 755 {} \\;");

        int failures = 0;
        for (String command : commands) {
            String[] cmd = {"/bin/sh", "-c", command};
            try {
                Process p = Runtime.getRuntime().exec(cmd);
                LOGGER.info("Command executed successfuly: " + command);
                
            } catch (IOException ex) {
                LOGGER.error("Command failed: " + command + ". Error message: " + ex.getMessage());
                failures++;
            }
        }
        
        if (failures > 0) {
            this.dbLog.add(new String[]{"isSuccess", "0"});
        } else {
            this.dbLog.add(new String[]{"isSuccess", "1"});
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void loadProperties() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            // load a properties file
            input = new FileInputStream("/usr/local/autodeploy/db.properties");
            prop.load(input);

            // get the property value and print it out
            this.USER = prop.getProperty("dbuser");
            this.PASS = prop.getProperty("dbpassword");
        } catch (IOException ex) {
            LOGGER.error("Failed to retrieve database credentials from /usr/local/autodeploy/db.properties. Error: " + ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    LOGGER.error("Failed to close file. Error: " + e.getMessage());
                }
            }
        }
    }

}
