#Git Auto-Deploy Java EE

##A Java EE web app for auto-deploying Bitbucket repos to your Linux server.

##Features:
- Auto-deploys the repo to your server when you push changes to the remote on Bitbucket
- Set the folder on your server where the repo will deploy e.g. `/var/www/html/mywebapp`
- Can deploy to different folders depending on the slug, branch and author. For example, only commits to the master branch get deployed. Or, commits by different authors get commited
to different folders etc.
- Makes a log of deployments (and failed attempts)

##Future features:
- Will work with Github as well

##How to use:

###Server setup:
1. Create a folder on your server `/usr/local/autodeploy`
2. Build a .war file from the project, copy to your server e.g. to the `/webapps` folder if using Tomcat

###Database setup:
1. Edit **dbsetup.sql** and **db.properties** and change the username and password to whatever you want
2. Run statements in dbsetup.sql i.e `mysql -u root -ppassword < dbsetup.sql`
3. Copy **db.properties** to `/usr/local/autodeploy`

###Repository setup:
1. Clone the Bitbucket repo on your server e.g. `/var/www/html/mywebapp`
2. Each repository will need a POST webhook setup which points to the location of the deploy app, for example *www.mydomain.com:8080/autodeploy*
3. Add the details of your repos to the **repositories** table in the database
4. When you commit to the repository, the webhook passes the repo details to the deploy app which triggers a git pull

##Issues?
- Make sure the folder on your server where the repo is cloned has the correct permissions
- Make sure `/usr/local/autodeploy` has correct permissions
- You'll need to have your SSH keys etc set correctly to ensure the server can pull commits